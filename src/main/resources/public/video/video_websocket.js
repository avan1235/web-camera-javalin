$(function() {

	const ws = new WebSocket("ws://@app_ip@:@app_port@/video");

	ws.onopen = function(e) {
		if (typeof console !== 'undefined') {
			console.info('Video websocket opened');
		}
	};

	ws.onmessage = function (e) {
		let data = JSON.parse(e.data),
			type = data.type,
			i = 0,
			cams = $('#webcams');

		let $img = null;
		let $card = null;
		let $card_body = null;
		let $card_title = null;

		if (typeof console !== 'undefined') {
			console.info('WS message', type);
		}

		if (type === 'list') {
			for (i = 0; i < data.webcams.length; i += 1) {
				$card_title = $( "<h4></h4>")
					.attr("class", "card-title");
				$card = $( "<div></div>")
					.attr("class", "card")
					.attr("style", "padding: 0; margin: 16px; float: center; position: center;");
				$card_body = $( "<div></div>")
					.attr("class", "card-body");
				$img = $("<img></img>")
					.attr("src", "load_webcam.jpg")
					.attr("style", "margin: 0; display: block; width: 100%; height: auto;")
					.attr("alt", data.webcams[i])
					.attr("name", data.webcams[i]);
				$card_title.append(data.webcams[i]);
				$card_body.append($card_title);
				$card_body.append($img);
				$card.append($card_body);
				cams.append($card);
			}
		}
		else if (type === 'image') {
			$img = $("img[name='" + data.webcam + "']")
				.attr("src", "data:image/jpeg;base64," + data.image)
				.trigger("change");
		}
	};

	ws.onclose = function() {
		if (typeof console !== 'undefined') {
			console.info('Video websocket closed');
		}
	};

	ws.onerror = function(err) {
		if (typeof console !== 'undefined') {
			console.info('Video websocket error');
		}
	};
});

