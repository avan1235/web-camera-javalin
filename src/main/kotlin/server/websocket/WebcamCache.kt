package server.websocket

import java.awt.image.BufferedImage
import java.util.ArrayList

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.github.sarxos.webcam.Webcam
import com.github.sarxos.webcam.WebcamEvent
import com.github.sarxos.webcam.WebcamListener
import com.github.sarxos.webcam.WebcamUpdater


object WebcamCache : WebcamUpdater.DelayCalculator, WebcamListener {

	private val LOG: Logger = LoggerFactory.getLogger(WebcamCache::class.java)

	/**
	 * How often images are updated on server.
	 */
	private const val DELAY: Long = 100

	fun getImage(name: String?): BufferedImage? {
		try {
			return webcams[name]?.image
		} catch (e: Exception) {
			LOG.error("Exception when getting image from webcam", e)
		}
		return null
	}

	val webcamNames: List<String>
		get() = webcams.keys.toList()

	private val webcams = mutableMapOf<String, Webcam>()

	private val handlers = mutableSetOf<WebcamSocketHandler>()

	init {
		for (cam in Webcam.getWebcams()) {
			cam.addWebcamListener(this)
			cam.open(true, this)
			webcams[cam.name] = cam
		}
	}

	override fun calculateDelay(snapshotDuration: Long, deviceFps: Double): Long {
		return (DELAY - snapshotDuration).coerceAtLeast(0)
	}

	fun subscribe(handler: WebcamSocketHandler) {
		handlers.add(handler)
	}

	fun unsubscribe(handler: WebcamSocketHandler?) {
		handlers.remove(handler)
	}

	override fun webcamOpen(event: WebcamEvent) { }

	override fun webcamClosed(event: WebcamEvent) { }

	override fun webcamDisposed(event: WebcamEvent) { }

	override fun webcamImageObtained(event: WebcamEvent) {
		for (handler in handlers) {
			handler.newImage(event.source, event.image)
		}
	}
}
