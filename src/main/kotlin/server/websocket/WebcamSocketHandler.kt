package server.websocket

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper

import com.github.sarxos.webcam.Webcam

import org.eclipse.jetty.websocket.api.Session
import org.eclipse.jetty.websocket.api.annotations.*

import org.slf4j.LoggerFactory

import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.*
import javax.imageio.ImageIO

@WebSocket
class WebcamSocketHandler {

    private var session: Session? = null

    private fun teardown() {
        session = try {
            session?.close()
            null
        } finally {
            WebcamCache.unsubscribe(this)
        }
    }

    private fun setup(session: Session) {
        this.session = session
        val message = mutableMapOf<String, Any>()
        message["type"] = "list"
        message["webcams"] = WebcamCache.webcamNames
        send(message)
        WebcamCache.subscribe(this)
    }

    @OnWebSocketClose
    fun onClose(status: Int, reason: String?) {
        LOG.info("WebSocket closed, status = {}, reason = {}", status, reason)
        teardown()
    }

    @OnWebSocketError
    fun onError(t: Throwable?) {
        LOG.error("WebSocket error", t)
        teardown()
    }

    @OnWebSocketConnect
    fun onConnect(session: Session) {
        LOG.info("WebSocket connect, from = ${session.remoteAddress.address}")
        setup(session)
    }

    @OnWebSocketMessage
    fun onMessage(message: String?) {
        LOG.info("WebSocket message, text = {}", message)
    }

    fun newImage(webcam: Webcam, image: BufferedImage?) {
        LOG.info("New image from {}", webcam)
        val stream = ByteArrayOutputStream()
        try {
            ImageIO.write(image, "JPG", stream)
        } catch (e: IOException) {
            LOG.error(e.message, e)
        }

        val base64 = String(Base64.getEncoder().encode(stream.toByteArray()), StandardCharsets.UTF_8)
        val message = mutableMapOf<String, Any?>()
        message["type"] = "image"
        message["webcam"] = webcam.name
        message["image"] = base64
        send(message)
    }

    private fun send(message: String) {
        session?.let {
            if (it.isOpen) {
                try {
                    it.remote?.sendStringByFuture(message)
                } catch (e: Exception) {
                    LOG.error("Exception when sending string", e)
                }
            }
        }
    }

    private fun send(message: Any) {
        try {
            send(MAPPER.writeValueAsString(message))
        } catch (e: JsonProcessingException) {
            LOG.error(e.message, e)
        }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(WebcamSocketHandler::class.java)
        private val MAPPER = ObjectMapper()
    }
}