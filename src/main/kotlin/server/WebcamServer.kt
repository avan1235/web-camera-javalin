package server

import io.javalin.Javalin
import io.javalin.http.util.RedirectToLowercasePathPlugin
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.websocket.server.WebSocketHandler
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory
import org.slf4j.LoggerFactory
import server.controller.AutoRemoveWebcamController
import server.controller.IWebcamController
import server.html.static.*
import server.websocket.WebcamSocketHandler

object WebcamServer {

    private val LOG = LoggerFactory.getLogger(WebcamServer::class.java)

    fun start(port: Int = 8080, webcamController: IWebcamController = AutoRemoveWebcamController()) {

        Javalin.create { config ->
            config.registerPlugin(RedirectToLowercasePathPlugin())
            config.showJavalinBanner = false
            config.server {
                Server().apply {
                    handler = object : WebSocketHandler() {
                        override fun configure(factory: WebSocketServletFactory) {
                            factory.register(WebcamSocketHandler::class.java)
                        }
                    }
                }
            }
        }.apply {
            get("/control/start/") { ctx ->
                ctx.html(StartRecordPage.content)
                webcamController.startRecords()
            }
            get("/control/stop/") { ctx ->
                ctx.html(StopRecordPage.content)
                webcamController.stopRecords()
            }
            get("/video/") { ctx ->
                ctx.html(VideoPage.content)
            }
            get("/control/") { ctx ->
                ctx.html(ControlPanelPage.content)
            }
            exception(Exception::class.java) { _, ctx ->
                ctx.html(ErrorPage.content)
            }
            get("/") { ctx ->
                ctx.redirect("/video/")
            }
            error(404) { ctx ->
                ctx.redirect("/")
            }
        }.start(port)
    }
}