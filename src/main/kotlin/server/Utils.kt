package server

import java.net.Inet4Address
import java.net.NetworkInterface

fun getIp(defaultIp: String = "127.0.0.1"): String {
    val ipRegex = """\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b""".toRegex()
    return NetworkInterface.getNetworkInterfaces().asSequence()
        .filter { it.isUp && !it.isLoopback &&  !it.isVirtual }
        .flatMap { it.inetAddresses.asSequence() }
        .filter { !it.isLoopbackAddress && it is Inet4Address }
        .mapNotNull { ipRegex.find(it.toString()) }
        .map { it.value }
        .firstOrNull() ?: defaultIp
}