package server.controller

import com.github.sarxos.webcam.Webcam
import webcam.Recorder
import webcam.WebcamDefaults
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class AutoRemoveWebcamController(private val checkMemory: Boolean = true) : IWebcamController {

    companion object {
        const val IMPORTANT_VIDEOS_COUNT = 10
    }

    private val recordTasks = mutableListOf<TimerTask>()

    override fun stopRecords() {
        recordTasks.forEach { it.cancel() }
        recordTasks.clear()
    }

    override fun takePicture() {
        TODO("Implement taking pictures in java")
    }

    override fun startRecordingSessions() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun stopRecordingSessions() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun startRecords(webcams: List<Webcam>) {
        removeNotImportantVideos()

        webcams.forEach { camera ->
            val path = currentTimePath(camera.name)
            recordTasks += Recorder.startRecordTask(camera, path)
        }
    }

    private fun currentTimePath(cameraName: String): String {
        val nameUnified = cameraName.toLowerCase().replace(" ", "_")
        val formattedTime = SimpleDateFormat(WebcamDefaults.fileTimeFormat).format(Date())
        return "${WebcamDefaults.videoPathPrefix}/${nameUnified}/VID_$formattedTime.${WebcamDefaults.videoFormat}"
    }

    private fun removeNotImportantVideos() {
        val mainFolder = File(WebcamDefaults.videoPathPrefix)
        mainFolder.mkdirs()

        if (checkMemory && (mainFolder.freeSpace.toDouble() / mainFolder.totalSpace.toDouble()) > 0.5) {
            return
        }

        mainFolder.listFiles(File::isDirectory)?.forEach { cameraDirectory ->
            cameraDirectory.listFiles()?.let { files ->
                files
                    .sortedByDescending { it.absolutePath }
                    .asSequence()
                    .drop(IMPORTANT_VIDEOS_COUNT)
                    .forEach { it.delete() }
            }
        }
    }
}