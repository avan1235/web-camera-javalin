package server.controller

import com.github.sarxos.webcam.Webcam

interface IWebcamController {

    fun startRecords(webcams: List<Webcam> = Webcam.getWebcams()): Unit

    fun stopRecords(): Unit

    fun takePicture(): Unit

    fun startRecordingSessions(): Unit

    fun stopRecordingSessions(): Unit
}