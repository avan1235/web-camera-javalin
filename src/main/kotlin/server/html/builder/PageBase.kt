package server.html.builder

import dev.scottpierce.html.writer.*
import dev.scottpierce.html.writer.element.*
import dev.scottpierce.html.writer.style.*
import java.time.LocalDate

@HtmlDsl
inline fun HtmlOutput.mainPage(navItemIndex: Int = -1, noinline headConf: BaseHtmlContext.() -> Unit = {}, mainContent: BodyContext.() -> Unit = {}) {
    html(DocType.Html, lang = "en") {
        headConfigured(headConf)
        body {
            navigation(
                listOf("../video", "../control"),
                listOf("Video Preview", "Control Panel"),
                navItemIndex = navItemIndex
                )
            mainConfigured(mainContent)
            footer()
        }
    }
}

@HtmlDsl
fun HtmlContext.headConfigured(headConf: BaseHtmlContext.() -> Unit = {}) {
    head {
        meta(
            "charset" to "utf-8",
            "name" to "viewport" ,
            "content" to "width=device-width, initial-scale=1, shrink-to-fit=no")

        link(rel = "stylesheet", href = "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons")
        link(rel = "stylesheet", href = "https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css")

        script(
            "integrity" to "sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN",
            "crossorigin" to "anonymous",
            "src" to "https://code.jquery.com/jquery-3.2.1.slim.min.js")

        script(
            "integrity" to "sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U",
            "crossorigin" to "anonymous",
            "src" to "https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js")

        script(
            "integrity" to "sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9",
            "crossorigin" to "anonymous",
            "src" to "https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js")

        headConf()

        title("Video system")
    }
}

@HtmlDsl
inline fun BodyContext.mainConfigured(mainContent: BodyContext.() -> Unit = {}) {
    main("class" to "container", "role" to "main") {
        div(classes = "row mt-5 pt-5") {
            div(classes = "col text-center") {
                mainContent()
            }
        }
    }
}

@HtmlDsl
fun BodyContext.navigation(linksMap: List<String>, linksNames: List<String>, navItemIndex: Int = 0) {
    nav(classes = "navbar fixed-top navbar-expand-lg navbar-dark bg-dark scrolling-navbar") {
        a(classes = "navbar-brand", href = "/") {
            +"Video System"
        }
        button(
            "class" to "navbar-toggler",
            "type" to "button",
            "data-toggle" to "collapse",
            "data-target" to "#navbarSupportedContent",
            "aria-controls" to "navbarSupportedContent",
            "aria-expanded" to "false",
            "aria-label" to "Toggle navigation"
        ) {
            span(classes = "navbar-toggler-icon")
        }
        div(id = "navbarSupportedContent", classes = "collapse navbar-collapse") {
            ul(classes = "navbar-nav mr-auto") {
                assert(linksMap.size == linksNames.size) { "Define the same links as names number" }

                repeat(linksMap.size) { id ->
                    li(classes = "nav-item${if (navItemIndex == id) " active" else ""}") {
                        a(classes = "nav-link", href = if (navItemIndex == id) "#" else linksMap[id]) {
                            +linksNames[id]
                            if (navItemIndex == id) {
                                span(classes = "sr-only") {
                                    +"(current)"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

fun BodyContext.footer() {
    footer(style = {
        position(Position.FIXED)
        bottom(Dimension.ZERO)
        width(100.percent)
        height(30.px)
        marginTop((-30).px)
        lineHeight(30.px)
        textAlign(TextAlign.CENTER)
        backgroundColor(Color("#f5f5f5"))
    }) {
        div(classes = "container") {
            span(classes = "text-muted") {
                +"Video System designed by Maciej Procyk - ${LocalDate.now().year}"
            }
        }
    }
}