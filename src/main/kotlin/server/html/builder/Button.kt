package server.html.builder

import dev.scottpierce.html.writer.BodyContext
import dev.scottpierce.html.writer.HtmlDsl
import dev.scottpierce.html.writer.element.button


@HtmlDsl
inline fun BodyContext.buttonCustom(
    classes: String,
    vararg attrs: Pair<String, String?>,
    id: String? = null,
    onClick: String? = null,
    func: BodyContext.() -> Unit = {}
) {
    button(mutableListOf(*attrs, "type" to "button").also {
        if (onClick != null) {
            it += "onclick" to onClick
        }
    }, id = id, classes = classes, func = func)
}

@HtmlDsl
inline fun BodyContext.buttonPrimary(
    vararg attrs: Pair<String, String?>,
    id: String? = null,
    onClick: String? = null,
    func: BodyContext.() -> Unit = {}
) = buttonCustom(
    classes = "btn btn-outline-primary waves-effect",
    attrs = *attrs,
    id = id,
    onClick = onClick,
    func = func
)

@HtmlDsl
inline fun BodyContext.buttonSecondary(
    vararg attrs: Pair<String, String?>,
    id: String? = null,
    onClick: String? = null,
    func: BodyContext.() -> Unit = {}
) = buttonCustom(
    classes = "btn btn-outline-secondary waves-effect",
    attrs = *attrs,
    id = id,
    onClick = onClick,
    func = func
)

@HtmlDsl
inline fun BodyContext.buttonGreen(
    vararg attrs: Pair<String, String?>,
    id: String? = null,
    onClick: String? = null,
    func: BodyContext.() -> Unit = {}
) = buttonCustom(
    classes = "btn btn-outline-success waves-effect",
    attrs = *attrs,
    id = id,
    onClick = onClick,
    func = func
)

@HtmlDsl
inline fun BodyContext.buttonRed(
    vararg attrs: Pair<String, String?>,
    id: String? = null,
    onClick: String? = null,
    func: BodyContext.() -> Unit = {}
) = buttonCustom(
    classes = "btn btn-outline-danger waves-effect",
    attrs = *attrs,
    id = id,
    onClick = onClick,
    func = func
)

