package server.html.script

import dev.scottpierce.html.writer.BaseHtmlContext
import dev.scottpierce.html.writer.HtmlDsl
import dev.scottpierce.html.writer.element.script
import java.io.File
import java.nio.charset.Charset

fun File.isJsScript(): Boolean {
    return extension == "js"
}

fun getResource(path: String): File {
    return File(object {}.javaClass.getResource(path).path)
}

@HtmlDsl
fun BaseHtmlContext.readScript(file: File, charset: Charset = Charsets.UTF_8) {
    script {
        +file.readText(charset)
    }
}

@HtmlDsl
fun BaseHtmlContext.readScripts(resourceDir: File, charset: Charset = Charsets.UTF_8, allowEmptyDir: Boolean = false) {
    assert(resourceDir.isDirectory && resourceDir.exists()) { "resourceDir for ScriptReader must be a valid directory" }

    val scriptFiles = resourceDir.listFiles()

    assert(allowEmptyDir || scriptFiles != null) { "Specified dir has no scripts but should have as it was defined" }

    scriptFiles
        ?.filter(File::isJsScript)
        ?.forEach { scriptFile ->
            readScript(scriptFile, charset)
        }
}

@HtmlDsl
fun BaseHtmlContext.readScriptsFromResource(path: String, charset: Charset = Charsets.UTF_8, allowEmptyDir: Boolean = false) {
    readScripts(getResource(path), charset, allowEmptyDir)
}