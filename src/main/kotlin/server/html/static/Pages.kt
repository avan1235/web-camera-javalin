package server.html.static

import dev.scottpierce.html.writer.StringHtmlOutput
import dev.scottpierce.html.writer.WriteOptions
import dev.scottpierce.html.writer.element.*
import dev.scottpierce.html.writer.element.td
import dev.scottpierce.html.writer.element.th
import server.html.builder.*
import server.html.script.readScriptsFromResource
import webcam.FileManager

interface StaticContentPage {
    val content: String
}

object VideoPage : StaticContentPage {

    override val content: String
        get() = StringHtmlOutput(options = WriteOptions.readable).apply {
            mainPage(navItemIndex = 0, headConf = { readScriptsFromResource("/public/video/") }) {
                div(id = "webcams")
            }
        }.toString()
}

object ControlPanelPage : StaticContentPage {
    override val content: String
        get() {
            return StringHtmlOutput(options = WriteOptions.readable).apply {
                mainPage(navItemIndex = 1) {
                    table(classes = "table table-sm") {
                        thead {
                            tr {
                                th(scope = "col") { +"Nr" }
                                th(scope = "col") { +"Filename" }
                                th(scope = "col") { +"Device" }
                                th(scope = "col") { +"Size [MB]" }
                            }
                        }
                        tbody {
                            val records = FileManager.listSavedRecords().sortedByDescending { it.name }
                            repeat(records.size) { id ->
                                tr {
                                    th { +"${id + 1}" }
                                    td { +records[id].name }
                                    td { +records[id].parentFile.name.replace('_', ' ') }
                                    td { +"${records[id].length() / 1_048_576}" }
                                }
                            }
                        }
                    }
                    buttonGreen(onClick = "location.href = './start';") {
                        +"Start recording"
                    }
                    buttonRed(onClick = "location.href = './stop';") {
                        +"Stop recording"
                    }
                }
            }.toString()
        }
}

object ErrorPage : StaticContentPage {

    override val content: String
        get() = StringHtmlOutput(options = WriteOptions.readable).apply {
            mainPage {
                h3 {
                    +"Internal server error"
                }
            }
        }.toString()
}

object StartRecordPage : StaticContentPage {

    override val content: String
        get() = StringHtmlOutput(options = WriteOptions.readable).apply {
            mainPage {
                h3 {
                    +"Starting recording..."
                }
            }
        }.toString()
}

object StopRecordPage : StaticContentPage {

    override val content: String
        get() = StringHtmlOutput(options = WriteOptions.readable).apply {
            mainPage {
                h3 {
                    +"Stopping recording..."
                }
            }
        }.toString()
}