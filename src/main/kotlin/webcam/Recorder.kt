package webcam

import com.github.sarxos.webcam.Webcam
import com.github.sarxos.webcam.WebcamResolution
import org.jcodec.api.awt.AWTSequenceEncoder
import org.jcodec.common.io.NIOUtils
import org.jcodec.common.model.Rational
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*

object Recorder {

    private val LOG: Logger = LoggerFactory.getLogger(Recorder::class.java)

    fun startRecordTask(webcam: Webcam, path: String, fps: Int = WebcamDefaults.FPS, size: WebcamResolution = WebcamDefaults.size, timer: Timer = Timer()): TimerTask {

        val file = File(path)
        file.parent?.let { File(it).mkdirs() }

        val out = NIOUtils.writableChannel(file)
        val encoder = AWTSequenceEncoder(out, Rational.R(fps, 1))
        val period = (1000.0 / fps.toDouble()).toLong()

        if (!webcam.isOpen) {
            webcam.viewSize = size.size
        }

        webcam.open(true)

        val task = object : TimerTask() {

            override fun run() {
                try {
                    webcam.image?.let { encoder.encodeImage(it) }
                }
                catch (e: Exception) {
                    LOG.error("Encoding error for frame on", e)
                }
            }

            override fun cancel(): Boolean {
                val result = super.cancel()
                encoder.finish()
                out.close()
                return result
            }
        }
        timer.schedule(task, 0, period)

        return task
    }
}

