package webcam

import com.github.sarxos.webcam.WebcamResolution
import java.nio.file.Paths

object WebcamDefaults {

    val FPS = 10
    val size = WebcamResolution.QVGA

    val videoPathPrefix = "${Paths.get("").toAbsolutePath()}/video"

    val fileTimeFormat = "yyyy_MM_dd_HH_mm_ss"
    val videoFormat = "mp4"

}