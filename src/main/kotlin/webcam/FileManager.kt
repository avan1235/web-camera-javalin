package webcam

import webcam.WebcamDefaults.videoPathPrefix
import java.io.File

object FileManager {

    fun listSavedRecords(): List<File> {
        return File(videoPathPrefix).walkTopDown().filter(File::isFile).toList()
    }
}