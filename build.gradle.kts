import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.apache.tools.ant.filters.*
import java.net.NetworkInterface
import java.net.Inet4Address

plugins {
    kotlin("jvm") version "1.3.61"
    id("com.github.johnrengelman.shadow") version "5.1.0"
    application
}

group = "com.procyk.maciej"
version = "1.0"

val RESOURCES_BUILD_DIR = "build/resources"
val RESOURCES_SRC_DIR = "src/main/resources"

repositories {
    mavenCentral()
    jcenter()
    maven(uri("http://www.dcm4che.org/maven2/"))
    maven(uri("http://oss.sonatype.org/content/repositories/snapshots"))
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.3")

    implementation("io.javalin:javalin:3.7.0")
    implementation("com.fasterxml.jackson.core:jackson-core:2.10.1")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.10.1")
    implementation("dev.scottpierce.kotlin-html:kotlin-html-writer-jvm:0.7.22")

    implementation("com.github.sarxos:webcam-capture:0.3.12")

    implementation("org.jcodec:jcodec:0.2.3")
    implementation("org.jcodec:jcodec-javase:0.2.3")

    implementation("org.slf4j:slf4j-api:1.8.0-beta4")
    implementation("org.slf4j:slf4j-simple:1.8.0-beta4")
}

sourceSets {
    main {
        resources {
            srcDirs(RESOURCES_BUILD_DIR)
        }
    }
}

fun getIp(defaultIp: String = "127.0.0.1"): String {
    val ipRegex = """\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b""".toRegex()
    return NetworkInterface.getNetworkInterfaces().asSequence()
        .filter { it.isUp && !it.isLoopback &&  !it.isVirtual }
        .flatMap { it.inetAddresses.asSequence() }
        .filter { !it.isLoopbackAddress && it is Inet4Address }
        .mapNotNull { ipRegex.find(it.toString()) }
        .map { it.value }
        .firstOrNull() ?: defaultIp
}

tasks {
    register("replaceResources", Copy::class) {

        val ip = getIp()
        val port = 8080
        println("Server address during configuration set to: $ip:$port")

        val tokens = mapOf(
            "app_ip" to ip,
            "app_port" to "$port")
        inputs.properties(tokens)

        from(RESOURCES_SRC_DIR) {
            filter<ReplaceTokens>("tokens" to tokens)
        }

        into(RESOURCES_BUILD_DIR)
        includeEmptyDirs = false
    }
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    processResources {
        dependsOn("replaceResources")
    }
}

tasks.withType<ShadowJar>() {
    manifest {
        attributes["Main-Class"] = "MainKt"
    }
}

application {
    mainClassName = "MainKt"
}