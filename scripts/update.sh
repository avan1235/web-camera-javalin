#!/bin/bash

ps -ef | grep 'server.jar' | grep -v grep | awk '{print $2}' | xargs -r kill -9

if [ "$1" == "" ]; then
    wget https://gitlab.com/avan1235/web-camera-javalin/-/jobs/artifacts/master/raw/build/libs/javalin-hello-1.0-all.jar?job=build
    mv ./'javalin-hello-1.0-all.jar?job=build' ./server.jar
fi

java -jar ./server.jar >>./log 2>&1 &